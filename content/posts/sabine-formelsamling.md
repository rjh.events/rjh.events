---
title: "Rumklang Formelsamling"
date: 2022-12-02T16:13:46+01:00
draft: false
---

Dette spreadsheet blev lavet til min svendeprøve til hjælp med akustikudregninger.

Formlen til højre i arkene er til manuelt at skrive sine variabler ind, den opdaterer ikke sig selv.

[Download 'Rumklang - Sabines Formel.ods'](<https://rjh.events/assets/Rumklang - Sabines Formel.ods>)

Jeg gør det frit tilgængeligt her under en [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) licens. Kort sagt vil det sige du er fri til at bruge, forbedre/ændre og distribuere denne fil, så længe du i filen anerkender mig som den originale skaber og deler den under den originale licens. 
Frihed på bekostning af du giver andre den samme frihed.

Tak til min underviser, Henrik Bonné.