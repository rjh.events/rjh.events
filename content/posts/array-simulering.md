---
title: "Array Simulering"
date: 2022-12-02T18:23:02+01:00
draft: false
---

- [NEXO’s NS1](https://www.nexo-sa.com/systems/software/)
  - [x] Windows
  - [ ] OSX
- [D&B ArrayCalc](https://www.dbaudio.com/global/en/products/software/arraycalc/)
  - [x] Windows
  - [x] OSX
- [L-Acoustics Soundvision](https://www.l-acoustics.com/products/soundvision/)
  - [x] Windows
  - [x] OSX
- [EASE Focus](https://www.afmg.eu/en/ease-focus)
  - [x] Windows
  - [ ] OSX
- [Meyer Sound MAPP XT](https://meyersound.com/product/mapp-xt/)
  - [x] Windows
  - [x] OSX
- [Meyer Sound MAPP 3D](https://meyersound.com/product/mapp-3d/)
  - [x] Windows
  - [x] OSX