**Jimmy Hansen**

**Mobil:** [+45 23 91 26 98](tel:+4523912698)

**Email:** [kontakt@rjh.events](mailto:kontakt@rjh.events)

---

Lydtekniker med stærke samarbejdsevner og teknisk know-how

---

Gem mine kontaktinformationer! Skan koden eller [download](Jimmy_Hansen_-_RJH_Event_Teknik.vcf).

![Contact Info QR-Code](images/contact-qr-white.svg) 
